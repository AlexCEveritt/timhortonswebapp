package sheridan;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

    @Test
    public void testDrinksRegular(){
	List<String> expected = MealsService.getAvailableMealTypes(MealType.DRINKS);
	assertEquals(expected, MealsService.getAvailableMealTypes(MealType.DRINKS));
	
    }
    @Test 
    public void testDrinksException() {
	
	List<String> expected = MealsService.getAvailableMealTypes(null);
	assertFalse("invalid drink list", expected.get(0).equalsIgnoreCase("No Drinks Available"));
	
    }
    
    @Test
    public void testDrinksBoundaryOut() {
	List<String> expected = MealsService.getAvailableMealTypes(null);
	assertFalse("invalid drink list", expected.size() < 0);
    }
    @Test
    public void testDrinksBoundaryIn() {
	List<String> expected = MealsService.getAvailableMealTypes(MealType.DRINKS);
	assertTrue("invalid drink list", expected.size() > 3);
    }


}
